set nocompatible
set number
set nowrap
set laststatus=2
set cmdheight=2
set clipboard=unnamedplus
" set listchars=tab:><Space>
" set listchars=tab:!·,trail:·
" set invlist
set tags+=~/Documents/clocker/sw/afr/tags
" set tags=~/.ctags.d/tags
" set tags=~/Documents/clocker/sw/afr/amazon-freertos/vendors/espressif/esp-idf/tags
" set tags=~/Downloads/esp-idf/tags

filetype plugin indent on

set tabstop=4
set shiftwidth=4
set expandtab
"-------------------------------------------------------------------------------
" Color scheme
"-------------------------------------------------------------------------------

" Zenburn color scheme
colors zenburn

"-------------------------------------------------------------------------------
" Highlight
"-------------------------------------------------------------------------------

" Italic comment mode
highlight Comment cterm=italic

"-------------------------------------------------------------------------------
" Key bindings
"-------------------------------------------------------------------------------

" Auto parentheses bindings
inoremap ( ()<Left>
inoremap { {}<Left>
inoremap [ []<Left>
inoremap " ""<Left>
inoremap ' ''<Left>
inoremap < <><Left>

" Auto parentheses + semicolon bindings
inoremap (; ();<Left><Left>
inoremap {; {};<Left><Left>
inoremap [; [];<Left><Left>
inoremap "; "";<Left><Left>
inoremap '; '';<Left><Left>
inoremap <; <>;<Left><Left>

" Abc
inoremap <<Space> <<Space>
inoremap {<CR> {}<Left><CR><CR><Up><Tab>
