" This option has the effect of making Vim either more Vi-compatible,
" or make Vim behave in a more useful way.
set nocompatible

" Specifies for which events the bell will not be rung.
set belloff=all

" Influences the working of <BS>, <Del>, CTRL-W and CTRL-U in Insert mode.
set backspace=2

syntax on

" Show the line and column number of the cursor position, separated by a comma.
set ruler
" Same as :print, but precede each line with its line number.
set number
set nowrap
set laststatus=2
set cmdheight=2
set clipboard=unnamedplus
" set listchars=tab:><Space>
" set listchars=tab:!�,trail:�
" set invlist
set tags+=~/Documents/clocker/sw/afr/tags
" set tags=~/.ctags.d/tags
" set tags=~/Documents/clocker/sw/afr/amazon-freertos/vendors/espressif/esp-idf/tags
" set tags=~/Downloads/esp-idf/tags

filetype plugin indent on

" Number of spaces that a <Tab> in the file counts for.
set tabstop=4

" Number of spaces to use for each step of (auto)indent.
set shiftwidth=4

" In Insert mode: Use the appropriate number of spaces to insert a <Tab>.
set expandtab

"-------------------------------------------------------------------------------
" Color scheme
"-------------------------------------------------------------------------------

" Zenburn color scheme
colors zenburn

"-------------------------------------------------------------------------------
" Highlight
"-------------------------------------------------------------------------------

" Italic comment mode
highlight Comment cterm=italic gui=italic

"-------------------------------------------------------------------------------
" Key bindings
"-------------------------------------------------------------------------------

" Auto parentheses bindings
inoremap ( ()<Left>
inoremap { {}<Left>
inoremap [ []<Left>
inoremap " ""<Left>
inoremap ' ''<Left>
inoremap < <><Left>

" Auto parentheses + semicolon bindings
inoremap (; ();<Left><Left>
inoremap {; {};<Left><Left>
inoremap [; [];<Left><Left>
inoremap "; "";<Left><Left>
inoremap '; '';<Left><Left>
inoremap <; <>;<Left><Left>

" Abc
inoremap <<Space> <<Space>
inoremap {<CR> {}<Left><CR><CR><Up><Tab>

"-------------------------------------------------------------------------------
" 
"-------------------------------------------------------------------------------

if has('gui_running')
  set guioptions-=m  "remove menu bar
  set guioptions-=T  "remove toolbar
  set guioptions-=r  "remove right-hand scroll bar
  set guioptions-=L  "remove left-hand scroll bar
  if has('gui_win32')
    set guifont=JetBrains_Mono:h18:cANSI
"    "run the command immediately when starting vim
"    autocmd GUIEnter * call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)
"    call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)
"
"    " activate/deactivate full screen with function key <F11>  
"    nnoremap <F11> :call libcallnr("gvimfullscreen.dll", "ToggleFullScreen", 0)<CR>
  else
    set guifont=JetBrains\ Mono\ 20
  endif
endif
